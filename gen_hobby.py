from os import listdir
from os.path import join, isdir, isfile, basename, dirname
from datetime import date
from PIL import Image

blog_entry = '''---
title: '%s'
date: %s
draft: false
tags: [hobby, %s]
image: %s
---

![](%s)
'''

img_dir = join("static", "img", "hobby")
blog_dir = join("content", "blog")
today = date.today().strftime("%Y-%m-%d")
miniature_width = 300

def resize_image(img_path, miniature_name):
    image = Image.open(img_path)
    w = image.width
    r = miniature_width/w
    miniature = image.resize((miniature_width, int(r * image.height)))
    print('Saving miniature %s' % miniature_name)
    miniature.save(miniature_name)

def make_blog(section_name, img_path):
    img = basename(img_path)
    img_name = img.split('.')[0]
    img_ext = img.split('.')[1]

    # Checking if blog entry already exists
    blog_file = img_name + '.md'
    blog_path = join(blog_dir, section_name, blog_file)
    if isfile(blog_path):
        print('Blog entry %s already exists' % blog_file)
        return

    # Creating miniature picture
    miniature_name = join(dirname(img_path), img_name + '_mini.' + img_ext)
    resize_image(img_path, miniature_name)

    entry = blog_entry % (
        img_name.replace('_',' '),
        today,
        section_name,
        miniature_name.replace('static', '/home'),
        img_path.replace('static', '/home')
    )

    print('Writing into %s :\n%s\n' % (blog_path, entry))
    f = open(blog_path, 'w')
    f.write(entry)
    f.close()

for dir in [d for d in listdir(img_dir) if isdir(join(img_dir, d))]:
    dir_path = join(img_dir, dir)
    for file in [f for f in listdir(dir_path) if isfile(join(dir_path, f)) and '_mini' not in f]:
        make_blog(dir, join(dir_path, file))
