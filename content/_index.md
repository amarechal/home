---
title: "Home"
date: 2018-02-10T18:56:13-05:00
sitemap:
  priority : 1.0

outputs:
- html
- rss
- json
---
I am currently working as a postdoctoral researcher in computer science at the Verimag laboratory.  
I have a [PhD](https://hal.archives-ouvertes.fr/tel-01695086) from Université Grenoble Alpes about program analysis, supervised by [Michaël Périn](http://www-verimag.imag.fr/~perin/) and [David Monniaux](http://www-verimag.imag.fr/~monniaux/).

My favorite subject is __algorithmics__ and all of its aspects (design, formal proof, complexity).  
The topics on which I worked are
* __Program analysis__, through formal methods (static analysis)
* __Polyhedral algorithmics__, which involves developing operators on geometric objects (a convex polyhedron being the intersection of a finite set of half-spaces)
* __Formal proof__ of programs, using proof assistants like Coq
* __Combinatorial optimization__, especially the simplex algorithm

I have a lot of experience in __programming__, mainly with __OCaml__, __Python__, __Java__, __C__, __C++__ and __Coq__. I participated in the development of several research projects:
* Verified Polyhedra Library, a library of polyhedra calculus
* Mopsa, a static analyzer of C and Python programs
* AbSolute, a tool for Constraint Programming

In parallel with my research, I teached about 300 hours at various levels at Université Grenoble Alpes, Ensimag, Polytech' Grenoble and Université Pierre et Marie Curie. The subjects were algorithmics, logic, compilation and programming.
