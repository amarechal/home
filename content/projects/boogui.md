---
title: "Boogui"
featured: true
weight: 4
image: "/home/img/boo.png"
description: "<a href=https://github.com/jbourgin/Boogui>Boogui</a> is a software dedicated to viewing and processing gaze data recorded with EyeLink or SMI eyetrackers.

<br>Boogui currently includes:
<ul>
    <li>Visualization of gaze data over time for each trial with target frames displayed</li>
    <li>A video view that plays back the trial with eye movements overlaid</li>
    <li>Filtering of SMI data based on fixation / saccade / blink parameters (e.g. duration). Compared to the default algorithm provided by SMI, it takes into account artifacts and allows to consider both dispersion and velocity for more accurate calculation of fixations.</li>
    <li>Output reports (.txt or .csv files) that can then be directly imported in statistical analysis packages such as Statistica or R for further processing.</li>
</ul>
"

tags: ["eyetracking"]
---
