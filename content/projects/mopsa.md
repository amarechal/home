---
title: "MOPSA"
featured: true
weight: 2
image: "/home/img/mopsa.png"
description: "<a href=https://mopsa.lip6.fr/>MOPSA</a> (Modular Open Platform for Static Analysis) is a research project aiming at developing effective methods and tools to make computer software more reliable.

<br>
MOPSA has an original way of analyzing programs with \"optional variables\" (e.g. a malloc in a conditional statement).
I worked on specific polyhedral operators to help such analyzes.

<br><br>Mopsa is funded by the
<a href=https://erc.europa.eu/>European Research Council</a> and hosted at
<a href=https://www.lip6.fr/>LIP6</a>"
tags: [ "abstract interpretation", "ocaml"]
---
