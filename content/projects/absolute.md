---
title: "AbSolute"
featured: true
weight: 3
image: "/home/img/absolute.png"
description: "<a href=https://github.com/mpelleau/AbSolute>AbSolute</a> is a constraint solver based on abstract domains from the theory of abstract interpretation.

<br><br>AbSolute is funded by ANR
<a href=https://www.anr-coverif.fr/>CoVerif</a>."
tags: [ "constraint programming", "ocaml"]
---
