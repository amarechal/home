---
title: "Verified Polyhedra Library"
featured: true
weight: 1
image: "/home/img/vpl.jpg"
description: "The <a href=https://github.com/VERIMAG-Polyhedra/VPL>Verified Polyhedra Library</a> (VPL) is a library combining OCaml, C++ and Coq for computing with convex polyhedra.
It provides standard operators -- certified in Coq -- to use this library as an abstract domain of polyhedra.

<br>I implemented a big part of the OCaml library, regarding
<ul>
<li>polyhedral operators based on parametric linear programming;</li>
<li>the link between OCaml and Coq
</ul>
For more details, refer to my <a href='https://hal.archives-ouvertes.fr/tel-01695086'>thesis</a>.

<br><br> VPL is developed at <a href='https://www-verimag.imag.fr/'>Verimag</a> and supported by ANR <a href=http://verasco.imag.fr/wiki/Main_Page>Verasco</a> and ERC <a href=http://stator.imag.fr/w/index.php/Main_Page>Stator</a>.
<br><b>Github link</b>: <a href='https://github.com/VERIMAG-Polyhedra/VPL'>https://github.com/VERIMAG-Polyhedra/VPL</a>
"
onlinedoc: "/projects/vpl/vpl-core/"
onlinename: "vpl-core"
tags: [ "polyhedra", "algorithimics", "parametric linear programming", "VPL", "ocaml", "coq", "c++"]
---
