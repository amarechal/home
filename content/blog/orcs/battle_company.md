---
title: '`Tyre Burners`: Orc Battle Company'
date: 2020-09-12
draft: false
tags: [hobby, orcs, Lord of the Rings]
image: /home/img/hobby/orcs/battle_company_mini.jpg
---

![](/home/img/hobby/orcs/battle_company.jpg)

More about them on [Battle-Companies-Manager](https://battle-companies-manager.com/company/2812).
